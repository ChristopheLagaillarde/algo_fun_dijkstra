# 🚀Dijkstra

## 📄Description

This is a project to calculate the shortest path from a node to the rest of the nodes of a graph.

## ⚙️ Installation

clone the repository (git must be installed)
```
git clone https://gitlab.com/ChristopheLagaillarde/algo_fun_dijkstra.git 
```
or you can install it using pip

```
pip install algo-fun-dijkstra
```

or take the gz file on gitlab and use this command

```
tar -xzf algo_fun_dijkstra-0.2.0.tar.gz
```

## ✅Verification

You can have my public key on here: 
https://keyserver.ubuntu.com/pks/lookup?search=309F4D460B9BF1BD8DD31EAB39A61CA659ADD6E3&fingerprint=on&op=index

Take it and to verify that the integrity and authenticity of my project. You should be able to use this command to do so. 

```
gpg --verify algo_fun_dijkstra-0.2.0.tar.gz.asc algo_fun_dijkstra-0.2.0.tar.gz
```

## 🕹️Usage

You have to import it using this command

```
from algo_fun.get_dijkstra_shortest_path import get_dijkstra_shortest_path
```

You can use the function as follow

```
 graph: dict[str, list[tuple]] = {
	   'A': [('B', 4), ('C', 5)],
	   'B': [('A', 4), ('C', 11), ('D', 9), ('E', 7)],
	   'C': [('A', 5), ('B', 11), ('E', 3)],
	   'D': [('B', 9), ('E', 13), ('F', 2)],
	   'E': [('B', 7), ('C', 3), ('D', 13), ('F', 6)],
	   'F': [('D', 2), ('E', 6)]
	}
get_dijkstra_shortest_path(graph, "A")
```

## 📝LICENSE
Copyright (c) 2024 Christophe Lagaillarde 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
